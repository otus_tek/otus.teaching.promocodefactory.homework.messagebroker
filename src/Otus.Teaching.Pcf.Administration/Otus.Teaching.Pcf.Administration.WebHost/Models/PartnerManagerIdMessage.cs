﻿using System;

namespace Otus.Teaching.Pcf.Common
{
    public class PartnerManagerIdMessage
    {
        public Guid? PartnerManagerId { get; set; }
    }
}